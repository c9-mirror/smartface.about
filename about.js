define(function(require, exports, module) {
    "use strict";
    main.consumes = ["ui", "commands", "Plugin", "Dialog"];
    main.provides = ["smartface.about"];
    module.exports = main;

    function formatString(str) {
        var args = Array.prototype.slice.call(arguments).slice(1);
        return str.replace(/{(\d+)}/g, function(match, number) {
            return typeof args[number] != 'undefined' ? args[number] : match;
        });
    }

    function main(options, imports, register) {
        var commands = imports.commands;
        var staticPrefix = options.staticPrefix;
        var ui = imports.ui;
        var Dialog = imports.Dialog;
        var Plugin = imports.Plugin;
        
        ui.insertCss(require("text!./about.less"), staticPrefix, plugin);
        
        var plugin = new Dialog("Smartface", main.consumes, {
            allowClose: false,
            width: "591px",
            height: "412px",
            heading: false,
            class: "smartface-about",
            name: "Smartface About",
            title: ""
        });
        
        var loaded = false;
        var htmlContent;
        plugin.on("load", function(e) {
            if (loaded) return false;
            loaded = true;

            commands.addCommand({
                name: "smf.about",
                isAvailable: function() {
                    return true;
                },
                exec: function() {
                    plugin.show();
                }
            }, plugin);
                        
            htmlContent = require("text!./about.html");
            var builtNo = "20150827";
            var developers = "Alper Özışık, Antti Panula, Burak Gül, Doruk Coşkun, Emre Balkanlı, Elhassan Wanas, " + 
                "Halit Karakış, Hüseyin Elmas, Merve Bıçakçı, Nosaiba Darwish, Osman Kibar, Serkan Serttop, Tolga Haliloğlu,  Osman Çelik";

            htmlContent = formatString(htmlContent, staticPrefix, builtNo, developers);
            
        });
        plugin.on("unload", function(e) {
            htmlContent = null;
            loaded = false;
            drawn = false;
        });

        var drawn = false;
        plugin.on("draw", function(e) {
            if (drawn) return;
            drawn = true;
            ui.insertHtml(e.html, htmlContent, plugin);
            var closeButton = document.querySelector(".smf-about-close");
            closeButton.onmouseup = function () {
                plugin.hide();  
            };
        });

        plugin.freezePublicAPI({});

        register(null, {
            "smartface.about": plugin
        });
    }
    return main;
});